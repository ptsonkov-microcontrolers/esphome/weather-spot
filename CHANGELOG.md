## v2.0
- Support for OTA firmware upgrade with http_request (device pull upgrades). Cable upgrade still available
- Add battery powered outside weather spot
- Introduce new structure for shared components

## v1.0
Initial release with regular structure, shared components and cable update  
Devices:
- Livingroom
- Bedroom
- Kidroom
- Storeroom