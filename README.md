# ESPHome Weather spots

After project clone copy  
_secrets.yaml.example_  
to  
_secrets.yaml_  
and update with correct values

## Prepare and use ESPHome CLI
https://esphome.io/guides/getting_started_command_line.html  
https://esphome.io/guides/getting_started_command_line.html#adding-some-features

x. Create Pyrhon virtual environment
```
# python -m venv /data/venv/esphome
```

x. Configure virtual environment
```
# source /data/venv/esphome/bin/activate
# pip install --upgrade pip
# pip install esphome
```

x. Work with ESPHome
```
# cd /path/to/esphomeWorkDir
```
- create new device
```
# esphome wizard test.yml
```
- all with one command (validate, build, upload, see process logs)
```
# esphome run test.yml
```

x. Separate commands
- validate config (after add new features)
```
# esphome config test.yml
```
- build firmware
```
# esphome compile test.yml
```
- upload firmware
```
# esphome upload test.yml
```

## Update and generate encryption key
https://community.home-assistant.io/t/2023-2-esphome-deprecated-api-password-how-to-update-to-encryption-key/528886  
https://esphome.io/components/api.html?highlight=randomly%20generated#configuration-variables

## Prepare and run OTA firmware server

OTA firmware server is available in **https://gitlab.com/ptsonkov-development/python/ota_server** as separate application, which can run as HomeAssistant add-on, standalone application or docker container, or Kubernetes deployment.
